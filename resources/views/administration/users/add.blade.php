@extends('app')

@section('content')

	<h3>
	Add User
	</h3>
	<hr/>
	{!! Form::open(['url' => 'administration/user/store', 'class' => 'form-horizontal']) !!}
		<div class="form-group">
			{!! Form::label('first_name', 'First Name', ['class' => 'col-md-4 control-label']) !!}
			<div class="col-md-6">
				{!! Form::text('first_name', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('last_name', 'Last Name', ['class' => 'col-md-4 control-label']) !!}
			<div class="col-md-6">
				{!! Form::text('last_name', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('email', 'E-Mail', ['class' => 'col-md-4 control-label']) !!}
			<div class="col-md-6">
				{!! Form::email('email', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('role', 'Role', ['class' => 'col-md-4 control-label']) !!}
			<div class="col-md-6">
				{!! Form::select('role', ['k1'=>'User', 'k2'=>'Administrator'] , Input::old('role_id')) !!}
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6 col-md-offset-4">
				{!! Form::submit('Add User', ['class' => 'btn btn-primary']) !!} 
			</div>
		</div>
	{!! Form::close() !!}

@endsection 
