@extends('app')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">Users Manager</div>

	<div class="panel-body">
		 <h3>
			<a href="{{ url('administration/user/create') }}" >{{ trans('csamazon.add_new_user') }} </a>
		</h3>
	</div>
</div>
@endsection